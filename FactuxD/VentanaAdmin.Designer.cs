﻿namespace FactuxD
{
    partial class VentanaAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblAdmin = new System.Windows.Forms.Label();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblCodigo = new System.Windows.Forms.Label();
            this.lblUsAdmin = new System.Windows.Forms.Label();
            this.lblNomAd = new System.Windows.Forms.Label();
            this.lblCodig = new System.Windows.Forms.Label();
            this.btnContenedor = new System.Windows.Forms.Button();
            this.btnAdministrar = new System.Windows.Forms.Button();
            this.btnCambiar = new System.Windows.Forms.Button();
            this.btnCerrar = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(297, 215);
            this.btnSalir.Size = new System.Drawing.Size(141, 23);
            // 
            // lblAdmin
            // 
            this.lblAdmin.AutoSize = true;
            this.lblAdmin.Location = new System.Drawing.Point(54, 30);
            this.lblAdmin.Name = "lblAdmin";
            this.lblAdmin.Size = new System.Drawing.Size(39, 13);
            this.lblAdmin.TabIndex = 0;
            this.lblAdmin.Text = "Admin:";
            this.lblAdmin.Click += new System.EventHandler(this.label1_Click);
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(54, 69);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(46, 13);
            this.lblUsuario.TabIndex = 1;
            this.lblUsuario.Text = "Usuario:";
            this.lblUsuario.Click += new System.EventHandler(this.label2_Click);
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Location = new System.Drawing.Point(54, 116);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(43, 13);
            this.lblCodigo.TabIndex = 2;
            this.lblCodigo.Text = "Codigo:";
            this.lblCodigo.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblUsAdmin
            // 
            this.lblUsAdmin.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblUsAdmin.Location = new System.Drawing.Point(132, 69);
            this.lblUsAdmin.Name = "lblUsAdmin";
            this.lblUsAdmin.Size = new System.Drawing.Size(113, 22);
            this.lblUsAdmin.TabIndex = 3;
            this.lblUsAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNomAd
            // 
            this.lblNomAd.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.lblNomAd.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblNomAd.Location = new System.Drawing.Point(132, 29);
            this.lblNomAd.Name = "lblNomAd";
            this.lblNomAd.Size = new System.Drawing.Size(113, 22);
            this.lblNomAd.TabIndex = 4;
            this.lblNomAd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCodig
            // 
            this.lblCodig.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCodig.Location = new System.Drawing.Point(132, 116);
            this.lblCodig.Name = "lblCodig";
            this.lblCodig.Size = new System.Drawing.Size(113, 22);
            this.lblCodig.TabIndex = 5;
            this.lblCodig.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnContenedor
            // 
            this.btnContenedor.Location = new System.Drawing.Point(297, 38);
            this.btnContenedor.Name = "btnContenedor";
            this.btnContenedor.Size = new System.Drawing.Size(141, 23);
            this.btnContenedor.TabIndex = 6;
            this.btnContenedor.Text = "Contenedor Principal";
            this.btnContenedor.UseVisualStyleBackColor = true;
            this.btnContenedor.Click += new System.EventHandler(this.btnContenedor_Click);
            // 
            // btnAdministrar
            // 
            this.btnAdministrar.Location = new System.Drawing.Point(297, 78);
            this.btnAdministrar.Name = "btnAdministrar";
            this.btnAdministrar.Size = new System.Drawing.Size(141, 23);
            this.btnAdministrar.TabIndex = 7;
            this.btnAdministrar.Text = "Administrar Usuarios";
            this.btnAdministrar.UseVisualStyleBackColor = true;
            // 
            // btnCambiar
            // 
            this.btnCambiar.Location = new System.Drawing.Point(297, 125);
            this.btnCambiar.Name = "btnCambiar";
            this.btnCambiar.Size = new System.Drawing.Size(141, 23);
            this.btnCambiar.TabIndex = 8;
            this.btnCambiar.Text = "Cambiar contraseña";
            this.btnCambiar.UseVisualStyleBackColor = true;
            // 
            // btnCerrar
            // 
            this.btnCerrar.Location = new System.Drawing.Point(297, 172);
            this.btnCerrar.Name = "btnCerrar";
            this.btnCerrar.Size = new System.Drawing.Size(141, 23);
            this.btnCerrar.TabIndex = 9;
            this.btnCerrar.Text = "Cerrar Sesion ";
            this.btnCerrar.UseVisualStyleBackColor = true;
            this.btnCerrar.Click += new System.EventHandler(this.button4_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(57, 172);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(171, 139);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // VentanaAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 342);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnCerrar);
            this.Controls.Add(this.btnCambiar);
            this.Controls.Add(this.btnAdministrar);
            this.Controls.Add(this.btnContenedor);
            this.Controls.Add(this.lblCodig);
            this.Controls.Add(this.lblNomAd);
            this.Controls.Add(this.lblUsAdmin);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.lblUsuario);
            this.Controls.Add(this.lblAdmin);
            this.Name = "VentanaAdmin";
            this.Text = "VentanaAdmin";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VentanaAdmin_FormClosed);
            this.Load += new System.EventHandler(this.VentanaAdmin_Load);
            this.Controls.SetChildIndex(this.lblAdmin, 0);
            this.Controls.SetChildIndex(this.lblUsuario, 0);
            this.Controls.SetChildIndex(this.lblCodigo, 0);
            this.Controls.SetChildIndex(this.lblUsAdmin, 0);
            this.Controls.SetChildIndex(this.lblNomAd, 0);
            this.Controls.SetChildIndex(this.lblCodig, 0);
            this.Controls.SetChildIndex(this.btnContenedor, 0);
            this.Controls.SetChildIndex(this.btnAdministrar, 0);
            this.Controls.SetChildIndex(this.btnCambiar, 0);
            this.Controls.SetChildIndex(this.btnCerrar, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.btnSalir, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAdmin;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblCodigo;
        private System.Windows.Forms.Label lblUsAdmin;
        private System.Windows.Forms.Label lblNomAd;
        private System.Windows.Forms.Label lblCodig;
        private System.Windows.Forms.Button btnContenedor;
        private System.Windows.Forms.Button btnAdministrar;
        private System.Windows.Forms.Button btnCambiar;
        private System.Windows.Forms.Button btnCerrar;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}